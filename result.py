# v.08APR19

import json

from init import *


class Result:

    def __init__(self, ok=None, success_msg=None, error_msg=None, data={}, misc={}):
        self.ok = ok
        self.success_msg = success_msg
        self.error_msg = error_msg
        self.data = data
        self.misc = misc
        self.msg = None

    def set(self, dataset, strict=True):
        if isinstance(dataset, str):
            dataset = json.loads(dataset)

        if isinstance(dataset, dict):
            for k, v in dataset.items():
                if strict:
                    if k in self.__dict__.keys():
                        setattr(self, k, v)
                else:
                    setattr(self, k ,v)
        self.process()

    def process(self):
        if self.ok is None:
            self.ok = 1 if (self.data or self.success_msg) else 0
        self.msg = self.success_msg if self.ok == 1 else self.error_msg
        return self

    @property
    def json(self):
        if not self.data:
            self.data = None
        if not self.misc:
            self.misc = None
        self.process()
        return json.dumps(self.__dict__, cls=C0JsonEncoder)
